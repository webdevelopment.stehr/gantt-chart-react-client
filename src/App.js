import React, { Component } from 'react';
import './App.css';
import TimeLine from "react-gantt-timeline"
import {config} from "./config/gantt-config";
import {getAllCampaigns, postCampaign, putCampaign} from "./api";
import GanttMenu from "./components/ganttMenu";
import moment from "moment";
import {pickRandomColor} from "./config/css-colors";
import InfoMessages from "./components/infoMessages"

class App extends Component {

    state = {
        campaigns: [],
        links: [],
        selectedCampaign: null,
        zoomMode: 'month',
        messageOpen: false,
        currentMessage: {}
    };

    componentDidMount() {
        // fix typography warnings thrown by material ui snackbar
        // found here: https://material-ui.com/style/typography/#migration-to-typography-v2
        window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
        getAllCampaigns().then(response => {
            console.info("get all campaigns - response: ", response);
            this.setState({
                campaigns: response.data.map(campaign => {
                    return {
                        id: campaign.Id,
                        name: campaign.Title,
                        start: campaign.StartDate,
                        end: campaign.EndDate,
                        color: campaign.Color
                    }
                })
            });
        }).catch(error => {
            console.info("get all campaigns - error: ", error);
            this.addMessage(error.toString());
        });
    }

    componentWillUnmount() {
        //TODO unmount component
    }

    //TODO
    // updating the client state after the (successful) response from backend api
    // this is causing some lag in the ui
    // solutions:
    // loading indicator
    // update the client data before the response and act and notify on error response
    // update the client data before backend responded, but add validation to client (chosen for now)
    onUpdateCampaign = (campaign, changedProps) => {
        const updatedCampaign = {
            ...campaign,
            ...changedProps
        };
        //simple client side validation on start and end date difference
        let timeDiff = moment.duration(moment(updatedCampaign.end).diff(moment(updatedCampaign.start)));
        if(timeDiff.asHours() >= 1){
            this.setState({ campaigns: this.state.campaigns.map(item => item.id === updatedCampaign.id ? updatedCampaign : item) });
            putCampaign({
                Id: updatedCampaign.id,
                Title: updatedCampaign.name,
                StartDate: moment(updatedCampaign.start).format(), //formatting dates with TimeShift to iso date format
                EndDate: moment(updatedCampaign.end).format(),
                Color: updatedCampaign.color
            }).then(response => {
                console.info("put campaign - response: ", response);
                this.addMessage("Campaign "+updatedCampaign.name+" successfully updated!");
            }).catch((error,c) => {
                console.info("put campaign - error: ", error);
                this.addMessage(error.toString());
            })
        }else {
            this.addMessage("Start and End date difference must be at least one hour.");
        }
    };

    onSelectCampaign = (campaign) => {
        this.setState({selectedCampaign: campaign})
    };

    //TODO description
    addCampaign = () => {
        let newCampaign = {
            Title: "new Campaign",
            StartDate: moment().format(),
            EndDate: moment().add(7,'days').format(),
            Color: pickRandomColor()
        };
        postCampaign(newCampaign).then(response  => {
            console.info("post campaign - response: ", response);
            this.addMessage("Campaign "+response.data.Title+" successfully added!");
            this.setState({campaigns: [...this.state.campaigns,{
                    id: response.data.Id,
                    name: response.data.Title,
                    start: response.data.StartDate,
                    end: response.data.EndDate,
                    color: response.data.Color,
                }]
            })
        }).catch(error => {
            console.info("post campaign - error: ", error);
            this.addMessage(error.toString());
        })
    };

    // the zoom function from react-gantt-timeline
    // is quite buggy in Week and Day mode
    onZoomChange = (event, value) => {
        this.setState({zoomMode: value})
    };

    render() {
        console.info("app campaign state: ", this.state.campaigns);
        return (
            <div className="App">
                <GanttMenu
                    onCampaignAdd={this.addCampaign}
                    zoomMode={this.state.zoomMode}
                    onZoomChange={this.onZoomChange}
                    onUpdateCampaign={this.onUpdateCampaign}
                />
                <TimeLine
                    config={config}
                    data={this.state.campaigns}
                    links={this.state.links}
                    onUpdateTask={this.onUpdateCampaign}
                    onSelectItem={this.onSelectCampaign}
                    selectedItem={this.state.selectedCampaign}
                    itemheight={40}
                    mode={this.state.zoomMode}
                />
                <InfoMessages
                    open={this.state.messageOpen}
                    message={this.state.currentMessage}
                    handleClose={this.handleMessageClose}
                    handleExited={this.handleMessageExited}
                />
            </div>
        );
    }

    /*
        messages via material ui snackbar with queue from
        this: https://material-ui.com/demos/snackbars/#consecutive-snackbars example
    */
    messageQueue = [];

    addMessage = (content) => {
        this.messageQueue.push({
            content,
            key: new Date().getTime(),
        })

        if(this.state.messageOpen)
            this.setState({messageOpen: false});
        else
            this.processQueue();
    };

    processQueue = () => {
        if(this.messageQueue.length > 0)
            this.setState({
                currentMessage: this.messageQueue.shift(),
                messageOpen: true
            })
    };

    handleMessageClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ messageOpen: false });
    };

    handleMessageExited = () => {
        this.processQueue();
    };
}

export default App;
