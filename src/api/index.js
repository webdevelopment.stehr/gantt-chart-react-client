import axios from "axios";

const apiClient = function() {
    const params = {
        headers: {
            'Content-Type': 'application/json ',
        },
        baseURL: "http://localhost:51965/api"
    };
    return axios.create(params);
};

/*GET all campaigns; returns AxiosPromise*/
export function getAllCampaigns(){
    return apiClient().get("/Campaign");
}

/*PUT campaign; returns AxiosPromise*/
export function putCampaign(campaign){
    return apiClient().put("/Campaign/"+campaign.Id,campaign);
}

/* POST (id less campaign) campaign; returns AxiosPromise*/
export function postCampaign(campaign){
    return apiClient().post("/Campaign",campaign);
}