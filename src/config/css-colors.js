export const fs_colorplate = [
    "#4d004d",
    "#A52A2A",
    "#5F9EA0",
    "#DC143C",
    "#008B8B",
    "#B8860B",
    "#FF8C00",
    "#008080",
    "#556B2F",
    "#DB7093",
    "#800080",
    //"#FFD700",
];

export function pickRandomColor() {
    return fs_colorplate[Math.floor(Math.random() * fs_colorplate.length)]
}