import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import React from "react";
import Button from "@material-ui/core/Button/Button";
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";

const GanttMenu = ({onCampaignAdd, zoomMode, onZoomChange}) => {

    return (
        <AppBar position="static" color="default">
            <Toolbar>
                <Button color="inherit" onClick={onCampaignAdd}>Add Campaign</Button>
                <Tabs value={zoomMode} onChange={onZoomChange}>
                    <Tab value="day" label="Day"/>
                    <Tab value="week" label="Week"/>
                    <Tab value="month" label="Month"/>
                    <Tab value="year" label="Year"/>
                </Tabs>
            </Toolbar>
        </AppBar>
    )
};

export default GanttMenu;