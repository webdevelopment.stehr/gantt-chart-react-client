import React from "react";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";

const InfoMessages = ({open,message,handleClose,handleExited}) => {
    return(
        <Snackbar
            open={open}
            anchorOrigin={{vertical:'bottom', horizontal:'left'}}
            autoHideDuration={2000}
            onClose={handleClose}
            onExited={handleExited}
            message={<span>{message.content}</span>}
        />
    )
};

export default InfoMessages